# EntityLocker

The task is to create a reusable utility class that provides synchronization mechanism similar to row-level DB locking.

The class is supposed to be used by the components that are responsible for managing storage and caching of different type of entities in the application. EntityLocker itself does not deal with the entities, only with the IDs (primary keys) of the entities.

Requirements:

1. EntityLocker should support different types of entity IDs.

2. EntityLocker’s interface should allow the caller to specify which entity does it want to work with (using entity ID), and designate the boundaries of the code that should have exclusive access to the entity (called “protected code”).

3. For any given entity, EntityLocker should guarantee that at most one thread executes protected code on that entity. If there’s a concurrent request to lock the same entity, the other thread should wait until the entity becomes available.

4. EntityLocker should allow concurrent execution of protected code on different entities.

Implemented bonus requirements:

I. Allow reentrant locking.

II. Allow the caller to specify timeout for locking an entity.

IV. Implement global lock. Protected code that executes under a global lock must not execute concurrently with any other protected code.

## Build & run tests
```
./gradlew test
```

## API design considerations
See API usage examples in `com.example.entitylocker.SingleThreadedBasicUsageTest`.

I wanted to create a relatively high-level API which won't require manual locks/unlocks handling from client code, similar to [Lock.withLock in Kotlin](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.concurrent/java.util.concurrent.locks.-lock/with-lock.html). But I also wanted to handle timeouts. 

I thought of three options:
1. Make regular methods `void` and methods with timeout `boolean`, similar to a familiar Java Lock API
2. Make it possible for all methods to return value, but throw TimeoutException if timeout happens
3. Always return a "wrapper" value which contains successful/unsuccessful status and optional result

I didn't want to force client code to unwrap this data each time. Also, because of the effectively final requirement for Java lambdas, sometimes it's hard to pass some data from void actions, e.g. `Runnable`, to the outside. So I decided to try option 2.

At first the methods accepted only `Function`/`Supplier`, but sometimes it forced client code to return dummy values or nulls. I added overloaded versions for `Consumer`/`Runnable` and renamed half of the methods to `compute...` and half to `run...` to avoid ambiguity in functional interfaces.  

## Memory usage
The main problem is that I couldn't think of a bullet-proof mechanism which will be both thread-safe and will cleanup 100% of unused objects in a timely manner. So I decided to remove locks periodically, using a scheduled background cleaner, and only remove the locks which weren't used for some time. This time has to be determined by the developer depending on e.g. typical timeouts.  

Obviously, this approach can cause problems (depending on amount of entities and overall throughput), but with the right numbers it should work well. Also it allows some tuning - e.g. if the amount of entities is small and doesn't change a lot, then scheduled cleaner can be disabled.

## Requirements
I assumed that if client code tries to acquire entity lock with specified timeout, then waiting on global lock should also count towards this timeout. The time spent waiting on global lock is subtracted from original timeout, and the inner entity lock is acquired using remaining, not original timeout. I think this is reasonable, because clients shouldn't care about the implementation details, usually they just want to stop the whole method from blocking for a long time.