package com.example.entitylocker;

import java.time.Duration;
import java.time.Instant;

/**
 * Locks from {@link EntityLocker}, which weren't used for some time, are periodically removed.
 * This class allows to fine tune this behavior, or to disable it altogether.
 */
public class LockCleanupPolicy {

    private static final Duration MAX_DURATION = Duration.ofSeconds(Long.MAX_VALUE);
    private static final LockCleanupPolicy NEVER = new LockCleanupPolicy(MAX_DURATION, MAX_DURATION);

    private final Duration cleanupPeriod;
    private final Duration staleLockThreshold;

    /**
     * Creates a new policy.
     * @param cleanupPeriod period between cleanups. Counted between starts, not between end of one and start of another.
     * @param staleLockThreshold if a lock for a particular entity hasn't been used for this amount of time, it will be removed.
     *                           Set it to a value larger than any reasonable timeout for your typical lock-using operations,
     *                           otherwise active keys may be affected.
     */
    public LockCleanupPolicy(Duration cleanupPeriod, Duration staleLockThreshold) {
        this.cleanupPeriod = cleanupPeriod;
        this.staleLockThreshold = staleLockThreshold;
    }

    /**
     * Same as {@link #LockCleanupPolicy(Duration, Duration)}, but cleanup will never run periodically.
     * You can do it manually via {@link EntityLocker#cleanupStaleLocks()},
     * optionally using information from {@link EntityLocker#activeLocksCount()}.
     */
    public static LockCleanupPolicy manual(Duration staleLockThreshold) {
        return new LockCleanupPolicy(MAX_DURATION, staleLockThreshold);
    }

    /**
     * Cleanup will never run periodically and you won't be able to remove locks via {@link EntityLocker#cleanupStaleLocks()}.
     */
    public static LockCleanupPolicy never() {
        return NEVER;
    }

    public Duration getCleanupPeriod() {
        return cleanupPeriod;
    }

    public Duration getStaleLockThreshold() {
        return staleLockThreshold;
    }

    boolean lockIsStale(TimestampedLock lock) {
        return Duration.between(lock.getLastUsed(), Instant.now()).compareTo(staleLockThreshold) >= 0;
    }

    boolean requiresScheduling() {
        return !cleanupPeriod.equals(MAX_DURATION);
    }

}
