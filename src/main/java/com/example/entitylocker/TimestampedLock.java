package com.example.entitylocker;

import java.time.Clock;
import java.time.Instant;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class TimestampedLock {

    private final Clock clock;
    private final Lock delegate;

    private volatile Instant lastUsed;

    TimestampedLock() {
        this(Clock.systemUTC());
    }

    TimestampedLock(Clock clock) {
        this.clock = clock;
        this.delegate = new ReentrantLock();
        updateLastUsed();
    }

    void lock() {
        updateLastUsed();
        delegate.lock();
    }

    boolean tryLock(long timeout, TimeUnit unit) throws InterruptedException {
        updateLastUsed();
        boolean lockSuccessful = delegate.tryLock(timeout, unit);
        if (lockSuccessful) {
            updateLastUsed();
        }
        return lockSuccessful;
    }

    void unlock() {
        delegate.unlock();
        updateLastUsed();
    }

    Instant getLastUsed() {
        return lastUsed;
    }

    private void updateLastUsed() {
        lastUsed = clock.instant();
    }
}
