package com.example.entitylocker;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Provides synchronization mechanism similar to row-level DB locking.
 * Designed to be used by the components that are responsible for managing storage and caching of different types of entities in the application.
 * Does not deal with the entities, only with the IDs (primary keys) of the entities.
 * @param <T> entity ID type. Should be suitable for use in HashMaps (have proper equals & hashCode implementation).
 */
public class EntityLocker<T> {

    private final Map<T, TimestampedLock> locks = new ConcurrentHashMap<>();

    private final ReadWriteLock globalLock = new ReentrantReadWriteLock();
    private final Lock globalReadLock = globalLock.readLock();
    private final Lock globalWriteLock = globalLock.writeLock();

    private final FixedNameThreadFactory threadFactory = new FixedNameThreadFactory("entity-locker-cleanup-scheduler");
    private final ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor(threadFactory);
    private final LockCleanupPolicy cleanupPolicy;

    /**
     * Constructs a new locker using specified cleanup policy.
     * @param cleanupPolicy see {@link LockCleanupPolicy} for possible options.
     */
    public EntityLocker(LockCleanupPolicy cleanupPolicy) {
        this.cleanupPolicy = cleanupPolicy;
        scheduleCleanup();
    }

    private void scheduleCleanup() {
        if (cleanupPolicy.requiresScheduling()) {
            long periodNanos = cleanupPolicy.getCleanupPeriod().toNanos();
            scheduledExecutor.scheduleAtFixedRate(this::cleanupStaleLocks, periodNanos, periodNanos, TimeUnit.NANOSECONDS);
        }
    }

    /**
     * Removes locks which weren't used for a certain amount of time, according to a chosen {@link LockCleanupPolicy}.
     */
    public void cleanupStaleLocks() {
        runWithGlobalLock(() -> locks.entrySet().removeIf(entry -> cleanupPolicy.lockIsStale(entry.getValue())));
    }

    /**
     * Executes specified action while holding a lock on an entity ID and releases it afterwards.
     * Use this method if you want to return some result.
     * @param entityId non-nullable entityId
     * @param entityFunction receives entityId and returns desired result
     * @param <R> result type
     */
    public <R> R computeWithLock(T entityId, Function<T, R> entityFunction) {
        Objects.requireNonNull(entityId, "Entity ID can't be null");

        globalReadLock.lock();

        try {
            TimestampedLock entityLock = getLock(entityId);
            entityLock.lock();

            try {
                return entityFunction.apply(entityId);
            } finally {
                entityLock.unlock();
            }
        } finally {
            globalReadLock.unlock();
        }
    }

    /**
     * Same as {@link EntityLocker#computeWithLock(Object, Function)}, but accepts timeout.
     * @throws TimeoutException if timeout exceeded
     * @throws InterruptedException if thread was interrupted
     */
    public <R> R computeWithLock(T entityId, Function<T, R> entityFunction, long timeout, TimeUnit unit) throws TimeoutException, InterruptedException {
        Objects.requireNonNull(entityId, "Entity ID can't be null");

        Instant globalWaitStarted = Instant.now();
        if (!globalReadLock.tryLock(timeout, unit)) {
            throw new TimeoutException(timeoutMessage(entityId, timeout, unit));
        }
        long usedTimeoutNanos = Duration.between(globalWaitStarted, Instant.now()).toNanos();
        long remainingTimeoutNanos = Math.max(0, unit.toNanos(timeout) - usedTimeoutNanos);

        TimestampedLock entityLock = getLock(entityId);
        try {
            if (!entityLock.tryLock(remainingTimeoutNanos, TimeUnit.NANOSECONDS)) {
                throw new TimeoutException(timeoutMessage(entityId, timeout, unit));
            }
            try {
                return entityFunction.apply(entityId);
            } finally {
                entityLock.unlock();
            }
        } finally {
            globalReadLock.unlock();
        }
    }

    /**
     * Executes specified action while holding a global lock, which isn't bound to any entity, and releases it afterwards.
     * Holding a global lock doesn't allow other EntityLocker lock-aware actions to run.
     * Use this method if you want to return some result.
     * @param resultSupplier returns desired result
     * @param <R> result type
     */
    public <R> R computeWithGlobalLock(Supplier<R> resultSupplier) {
        globalWriteLock.lock();

        try {
            return resultSupplier.get();
        } finally {
            globalWriteLock.unlock();
        }
    }

    /**
     * Same as {@link EntityLocker#computeWithGlobalLock(Supplier)}, but accepts timeout.
     * @throws TimeoutException if timeout exceeded
     * @throws InterruptedException if thread was interrupted
     */
    public <R> R computeWithGlobalLock(Supplier<R> resultSupplier, long timeout, TimeUnit unit) throws TimeoutException, InterruptedException {
        if (!globalWriteLock.tryLock(timeout, unit)) {
            throw new TimeoutException(timeoutMessage(timeout, unit));
        }
        try {
            return resultSupplier.get();
        } finally {
            globalWriteLock.unlock();
        }
    }

    /**
     * Same as {@link EntityLocker#computeWithLock(Object, Function)}, but is useful when you don't want to return anything from your action.
     */
    public void runWithLock(T entityId, Consumer<T> entityAction) {
        computeWithLock(entityId, toFunction(entityAction));
    }

    /**
     * Same as {@link EntityLocker#computeWithLock(Object, Function, long, TimeUnit)}, but is useful when you don't want to return anything from your action.
     */
    public void runWithLock(T entityId, Consumer<T> entityAction, long timeout, TimeUnit unit) throws TimeoutException, InterruptedException {
        computeWithLock(entityId, toFunction(entityAction), timeout, unit);
    }

    /**
     * Same as {@link EntityLocker#computeWithGlobalLock(Supplier)}, but is useful when you don't want to return anything from your action.
     */
    public void runWithGlobalLock(Runnable action) {
        computeWithGlobalLock(toSupplier(action));
    }

    /**
     * Same as {@link EntityLocker#computeWithGlobalLock(Supplier, long, TimeUnit)}, but is useful when you don't want to return anything from your action.
     */
    public void runWithGlobalLock(Runnable action, long timeout, TimeUnit unit) throws TimeoutException, InterruptedException {
        computeWithGlobalLock(toSupplier(action), timeout, unit);
    }

    /**
     * Can be used to monitor internal usage of locks, for example, if you want to manually cleanup them using {@link EntityLocker#cleanupStaleLocks()}
     */
    public int activeLocksCount() {
        return locks.size();
    }

    private Function<T, Void> toFunction(Consumer<T> action) {
        return (entityId) -> {
            action.accept(entityId);
            return null;
        };
    }

    private Supplier<Void> toSupplier(Runnable action) {
        return () -> {
            action.run();
            return null;
        };
    }

    private TimestampedLock getLock(T key) {
        return locks.computeIfAbsent(key, k -> new TimestampedLock());
    }

    private String timeoutMessage(T entityId, long timeout, TimeUnit unit) {
        return String.format("Failed to get lock on '%s' within %d %s", entityId, timeout, unit.toString().toLowerCase());
    }

    private String timeoutMessage(long timeout, TimeUnit unit) {
        return String.format("Failed to get global lock within %d %s", timeout, unit.toString().toLowerCase());
    }

    private class FixedNameThreadFactory implements ThreadFactory {
        private final String name;

        FixedNameThreadFactory(String name) {
            this.name = name;
        }

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, name);
        }
    }

}
