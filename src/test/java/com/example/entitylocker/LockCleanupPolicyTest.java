package com.example.entitylocker;

import org.junit.Test;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LockCleanupPolicyTest {

    private final LockCleanupPolicy fiveSecondPolicy = LockCleanupPolicy.manual(Duration.ofSeconds(5));

    @Test
    public void shouldDetectStaleLockIfUsedLongAgo() {
        Clock minuteAgo = Clock.fixed(Instant.now().minus(1, ChronoUnit.MINUTES), ZoneId.of("UTC"));
        TimestampedLock lock = new TimestampedLock(minuteAgo);
        assertTrue(fiveSecondPolicy.lockIsStale(lock));
    }

    @Test
    public void shouldNotDetectStaleLockIfUsedRecently() {
        Clock secondAgo = Clock.fixed(Instant.now().minus(1, ChronoUnit.SECONDS), ZoneId.of("UTC"));
        TimestampedLock lock = new TimestampedLock(secondAgo);
        assertFalse(fiveSecondPolicy.lockIsStale(lock));
    }

    @Test
    public void shouldRequireSchedulingIfPeriodLessThanMaxDuration() {
        LockCleanupPolicy standardPolicy = new LockCleanupPolicy(Duration.ofMinutes(5), Duration.ofSeconds(30));
        assertTrue(standardPolicy.requiresScheduling());
    }

    @Test
    public void shouldNotRequireSchedulingIfPeriodEqualToMaxDuration() {
        LockCleanupPolicy manualPolicy = LockCleanupPolicy.manual(Duration.ofSeconds(100));
        LockCleanupPolicy neverCleaningPolicy = LockCleanupPolicy.never();
        assertFalse(manualPolicy.requiresScheduling());
        assertFalse(neverCleaningPolicy.requiresScheduling());
    }
}