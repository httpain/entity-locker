package com.example.entitylocker;

import org.junit.Test;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;

public class LockTimeoutTest {

    private static final int ENTITY_ID = 1;

    private final EntityLocker<Integer> entityLocker = new EntityLocker<>(LockCleanupPolicy.never());
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private final CountDownLatch firstLatch = new CountDownLatch(1);
    private final CountDownLatch secondLatch = new CountDownLatch(1);

    @Test
    public void shouldSucceedIfEntityBlockedLessThanTimeout() throws Exception {
        setupGlobalBackgroundThread(Duration.ofMillis(100));

        firstLatch.countDown();
        secondLatch.await();
        entityLocker.runWithLock(ENTITY_ID, id -> { }, 500, TimeUnit.MILLISECONDS);
    }

    @Test(expected = TimeoutException.class)
    public void shouldThrowIfCannotAcquireEntityLock() throws Exception {
        setupEntityBackgroundThread(Duration.ofMillis(200));
        expectEntityLockTimeout();
    }

    @Test(expected = TimeoutException.class)
    public void shouldThrowIfCannotAcquireGlobalLock() throws Exception {
        setupGlobalBackgroundThread(Duration.ofMillis(200));
        expectGlobalLockTimeout();
    }

    @Test(expected = TimeoutException.class)
    public void shouldThrowIfEntityLockBlockedByGlobalLock() throws Exception {
        setupGlobalBackgroundThread(Duration.ofMillis(200));
        expectEntityLockTimeout();
    }

    private void setupGlobalBackgroundThread(Duration duration) {
        executor.submit(() -> {
            firstLatch.await();
            entityLocker.runWithGlobalLock(() -> {
                secondLatch.countDown();
                pretendToWork(duration);
            });
            return null;
        });
    }

    private void setupEntityBackgroundThread(Duration duration) {
        executor.submit(() -> {
            firstLatch.await();
            entityLocker.runWithLock(ENTITY_ID, id -> {
                secondLatch.countDown();
                pretendToWork(duration);
            });
            return null;
        });
    }

    private void expectEntityLockTimeout() throws InterruptedException, TimeoutException {
        firstLatch.countDown();
        try {
            secondLatch.await();
            entityLocker.computeWithLock(ENTITY_ID, id -> "some result", 0, TimeUnit.NANOSECONDS);
        } catch (TimeoutException e) {
            assertEquals("Failed to get lock on '1' within 0 nanoseconds", e.getMessage());
            throw e;
        }
    }

    private void expectGlobalLockTimeout() throws InterruptedException, TimeoutException {
        firstLatch.countDown();
        try {
            secondLatch.await();
            entityLocker.computeWithGlobalLock(() -> "some result", 0, TimeUnit.NANOSECONDS);
        } catch (TimeoutException e) {
            assertEquals("Failed to get global lock within 0 nanoseconds", e.getMessage());
            throw e;
        }
    }

    private void pretendToWork(Duration duration) {
        try {
            Thread.sleep(duration.toMillis());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
