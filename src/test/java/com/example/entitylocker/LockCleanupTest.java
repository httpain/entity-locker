package com.example.entitylocker;

import org.junit.Test;

import java.time.Duration;

import static org.junit.Assert.assertEquals;

public class LockCleanupTest {

    private static final String ENTITY_ID = "FOO";

    @Test
    public void shouldCleanupLocksOnDemandIfManualPolicy() {
        LockCleanupPolicy manualPolicy = LockCleanupPolicy.manual(Duration.ofSeconds(0));
        EntityLocker<String> entityLocker = new EntityLocker<>(manualPolicy);
        entityLocker.runWithLock(ENTITY_ID, id -> { });
        assertEquals(Long.valueOf(1), Long.valueOf(entityLocker.activeLocksCount()));

        entityLocker.cleanupStaleLocks();
        assertEquals(Long.valueOf(0), Long.valueOf(entityLocker.activeLocksCount()));
    }

    @Test
    public void shouldCleanupLocksAutomaticallyIfScheduledPolicy() throws InterruptedException {
        LockCleanupPolicy scheduledPolicy = new LockCleanupPolicy(Duration.ofMillis(10), Duration.ofSeconds(0));
        EntityLocker<String> entityLocker = new EntityLocker<>(scheduledPolicy);
        entityLocker.runWithLock(ENTITY_ID, id -> { });
        assertEquals(Long.valueOf(1), Long.valueOf(entityLocker.activeLocksCount()));

        Thread.sleep(100);
        assertEquals(Long.valueOf(0), Long.valueOf(entityLocker.activeLocksCount()));
    }
}
