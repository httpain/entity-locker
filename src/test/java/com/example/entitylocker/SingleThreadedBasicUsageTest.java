package com.example.entitylocker;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SingleThreadedBasicUsageTest {

    private final Map<String, String> storage = new HashMap<String, String>() {
        {
            put("Alice", "A");
            put("Bob", "B");
        }
    };

    private final EntityLocker<String> entityLocker = new EntityLocker<>(LockCleanupPolicy.never());

    @Test
    public void shouldComputeWithLock() {
        String previousValue = entityLocker.computeWithLock("Alice", id -> storage.put(id, "AA"));
        assertEquals("A", previousValue);
        assertEquals("AA", storage.get("Alice"));
    }

    @Test
    public void shouldComputeWithLockAndTimeout() throws Exception {
        String previousValue = entityLocker.computeWithLock("Alice", id -> storage.put(id, "AA"), 1, TimeUnit.SECONDS);
        assertEquals("A", previousValue);
        assertEquals("AA", storage.get("Alice"));
    }

    @Test
    public void shouldComputeWithGlobalLock() {
        int size = entityLocker.computeWithGlobalLock(() -> storage.size());
        assertEquals(storage.size(), size);
    }

    @Test
    public void shouldComputeWithGlobalLockAndTimeout() throws Exception {
        int size = entityLocker.computeWithGlobalLock(() -> storage.size(), 15, TimeUnit.NANOSECONDS);
        assertEquals(storage.size(), size);
    }

    @Test
    public void shouldRunWithLock() {
        entityLocker.runWithLock("Alice", storage::remove);
        assertEquals(1, storage.size());
    }

    @Test
    public void shouldRunWithLockAndTimeout() throws Exception {
        entityLocker.runWithLock("Alice", storage::remove, 0, TimeUnit.MINUTES);
        assertEquals(1, storage.size());
    }

    @Test
    public void shouldRunWithGlobalLock() {
        entityLocker.runWithGlobalLock(storage::clear);
        assertTrue(storage.isEmpty());
    }

    @Test
    public void shouldRunWithGlobalLockAndTimeout() throws Exception {
        entityLocker.runWithGlobalLock(storage::clear, 100, TimeUnit.DAYS);
        assertTrue(storage.isEmpty());
    }

    @Test
    public void shouldRunReentrant() {
        entityLocker.runWithLock("Alice", id -> {
            entityLocker.runWithLock("Alice", innerId -> {
                storage.put("Alice", storage.get("Alice").concat("+"));
            });
            storage.put("Alice", storage.get("Alice").concat("+"));
        });
        assertEquals("A++", storage.get("Alice"));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWithMeaningfulMessageOnNullEntityId() {
        try {
            entityLocker.runWithLock(null, id -> {
            });
        } catch (NullPointerException e) {
            assertEquals("Entity ID can't be null", e.getMessage());
            throw e;
        } catch (Exception e) {
            fail("Unexpected exception thrown");
        }
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowWithMeaningfulMessageOnNullEntityIdWithTimeout() {
        try {
            entityLocker.runWithLock(null, id -> {
            }, 66, TimeUnit.MINUTES);
        } catch (NullPointerException e) {
            assertEquals("Entity ID can't be null", e.getMessage());
            throw e;
        } catch (Exception e) {
            fail("Unexpected exception thrown");
        }
    }

}