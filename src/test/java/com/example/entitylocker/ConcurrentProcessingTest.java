package com.example.entitylocker;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;

public class ConcurrentProcessingTest {

    private static final int THREADS = 10;
    private static final int ITERATIONS = 100_000;

    private final Map<String, Long> storage = new ConcurrentHashMap<>();
    private final EntityLocker<String> entityLocker = new EntityLocker<>(LockCleanupPolicy.never());
    private final ExecutorService executor = Executors.newFixedThreadPool(THREADS);

    @Test
    public void shouldIncrementSingleValueFromMultipleThreads() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        String entityId = "ID_1";

        List<Future<Void>> results = new ArrayList<>();
        for (int i = 0; i < THREADS; i++) {
            results.add(executor.submit(new Incrementer(entityId, latch)));
        }

        latch.countDown();
        for (Future result : results) {
            result.get();
        }

        assertEquals(Long.valueOf(THREADS * ITERATIONS), storage.get(entityId));
    }

    @Test
    public void shouldIncrementSingleValueFromMultipleThreadsWhileTimeoutsHappen() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        String entityId = "ID_1";

        List<Future<Long>> results = new ArrayList<>();
        for (int i = 0; i < THREADS; i++) {
            results.add(executor.submit(new TimedErrorReportingIncrementer(entityId, latch, 0, TimeUnit.NANOSECONDS)));
        }

        latch.countDown();

        long errors = 0;
        for (Future<Long> future : results) {
            errors += future.get();
        }

        assertEquals(Long.valueOf(THREADS * ITERATIONS - errors), storage.get(entityId));
    }

    @Test
    public void shouldIncrementMultipleValuesFromMultipleThreads() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        List<String> entityIds = Arrays.asList("ID_1", "ID_2", "ID_3");

        List<Future<Void>> results = new ArrayList<>();
        for (int i = 0; i < THREADS; i++) {
            // make threads use different entities in different order to mimic real life scenarios
            List<String> shuffledEntityIds = new ArrayList<>(entityIds);
            Collections.shuffle(shuffledEntityIds);
            results.add(executor.submit(new Incrementer(shuffledEntityIds, latch)));
        }

        latch.countDown();
        for (Future result : results) {
            result.get();
        }

        for (String entityId : entityIds) {
            assertEquals(Long.valueOf(THREADS * ITERATIONS), storage.get(entityId));
        }
    }

    private void incrementStorageValue(String id) {
        // replace Map#merge with multiple commands to make it as non-atomic as possible
        storage.putIfAbsent(id, 0L);
        storage.put(id, storage.get(id) + 1);
    }

    class Incrementer implements Callable<Void> {

        private final List<String> entityIds;
        private final CountDownLatch trigger;

        Incrementer(String entityId, CountDownLatch trigger) {
            this(Collections.singletonList(entityId), trigger);
        }

        Incrementer(List<String> entityIds, CountDownLatch trigger) {
            this.entityIds = entityIds;
            this.trigger = trigger;
        }

        @Override
        public Void call() throws Exception {
            trigger.await();
            for (int i = 0; i < ITERATIONS; i++) {
                for (String entityId : entityIds) {
                    entityLocker.runWithLock(entityId, id -> incrementStorageValue(id));
                }
            }
            return null;
        }
    }

    class TimedErrorReportingIncrementer implements Callable<Long> {

        private final List<String> entityIds;
        private final CountDownLatch trigger;
        private final long timeout;
        private final TimeUnit unit;

        TimedErrorReportingIncrementer(String entityId, CountDownLatch trigger, long timeout, TimeUnit unit) {
            this(Collections.singletonList(entityId), trigger, timeout, unit);
        }

        TimedErrorReportingIncrementer(List<String> entityIds, CountDownLatch trigger, long timeout, TimeUnit unit) {
            this.entityIds = entityIds;
            this.trigger = trigger;
            this.timeout = timeout;
            this.unit = unit;
        }

        @Override
        public Long call() throws Exception {
            trigger.await();
            long failures = 0;
            for (int j = 0; j < ITERATIONS; j++) {
                for (String entityId : entityIds) {
                    try {
                        entityLocker.runWithLock(entityId, id -> incrementStorageValue(id), timeout, unit);
                    } catch (TimeoutException | InterruptedException e) {
                        failures++;
                    }
                }
            }
            return failures;
        }
    }

}
